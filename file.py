from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains
from selenium.webdriver.support.ui import Select

import time
driver = webdriver.Chrome()
driver.get("https://admision.ipn.mx/sd/solicitud/")
content = driver.find_element_by_class_name('col-sm-offset2')
print(f"content: {dir(content)}")
print(f"content: {content.text}")
print(f"content: {content.get_attribute('innerHTML')}")

button = content.find_element_by_class_name('btn')
ActionChains(driver).click(button).perform()
time.sleep(2)
""" SEGUNDA PAGINA"""
# Rellenar curp
curp = driver.find_element_by_id('whCurp')
curp.send_keys("AIHJ950811HMCLRS03")
curp.send_keys(Keys.TAB)

# Insertar fecha
fecha = driver.find_element_by_id('whFecNacCfr')
fecha.send_keys("11/08/1995")
# Dar clic en continuar
fecha = driver.find_element_by_id('btContinuar')
fecha.send_keys(Keys.ENTER)
time.sleep(2)


""" TERCERA PAGINA"""
# Seleccionar estado civil
select = Select(driver.find_element_by_name('EdoCiv'))
# select by visible text
select.select_by_visible_text('Soltero(a)')
# select by value 
# select.select_by_value('1')

# Seleccionar servicio medico
select = Select(driver.find_element_by_name('SerMed'))
# select by visible text
select.select_by_value('4')

# Seleccionar descapacidad 
select = Select(driver.find_element_by_name('ImpFis'))
# select by visible text
select.select_by_value('0')

# Seleccionar lengua indigena 
select = Select(driver.find_element_by_name('whLangIn'))
# select by visible text
select.select_by_value('0')

# Insertar domicilio
# Codigo postal
curp = driver.find_element_by_id('whCodPos')
curp.send_keys("07880")
curp.send_keys(Keys.TAB)
# Calle
curp = driver.find_element_by_id('whCalle')
curp.send_keys("algo")

# Datos de contacto
# tel fijo
tel = driver.find_element_by_id('whTelefono')
tel.send_keys("5574878987")
#Confirmacion
tel = driver.find_element_by_name('cTel')
tel.send_keys("5574878987")
time.sleep(1)
#tel movil
movil = driver.find_element_by_id('whCelular')
movil.send_keys("5574878921")
#confirmacion
tel = driver.find_element_by_name('cCel')
tel.send_keys("5574878921")

# correo
mail = driver.find_element_by_id('whCorreo')
mail.send_keys("loreso.alfinti1234@hotmail.com")
#confirmacion
mail = driver.find_element_by_name('cMail')
mail.send_keys("loreso.alfinti1234@hotmail.com")
time.sleep(1)
# Boton continuar
fecha = driver.find_element_by_id('btContinuar')
fecha.send_keys(Keys.ENTER)
time.sleep(1)
""" CUARTA PAGINA"""
continuar = driver.find_element_by_name('btBach')
continuar.send_keys(Keys.ENTER)

"""QUINTA PAGINA"""

#Sleecionar entidad
for i in range(33):
    entidad = driver.find_element_by_name('EdoBach')
    select = Select(entidad)
    select.select_by_index(str(i))
    entidad.send_keys(Keys.TAB)
    time.sleep(1)
    for j in range(150):
        try:
            entidad = driver.find_element_by_name('InstProc')
            select = Select(entidad)
            select.select_by_index(str(j))
            entidad.send_keys(Keys.TAB)
            time.sleep(3)
            try:
                bacho = driver.find_element_by_name('Bachillerato')
                contenido_bacho = bacho.get_attribute('innerHTML')
                # if "15PBH3474B" in contenido_bacho:
                if "15PBH3474B" in contenido_bacho:
                
                    print(f"entidad: {i}, de institutos: {j} y data{contenido_bacho}")
            except:
                break
            
        except Exception as e:
            print("Que paso:")
            print(e)
            break


time.sleep(2)
# elem.send_keys("pycon")
# elem.send_keys(Keys.RETURN)
# assert "Python" in driver.title
# elem = driver.find_element_by_name("q")
# elem.clear()
# elem.send_keys("pycon")
# elem.send_keys(Keys.RETURN)
# assert "No results found." not in driver.page_source
driver.close()

# Add this to terminal before execute file
# export PATH=$PATH:/home/jesus/Projects/scrapping